package au.com.harrygill;

import java.util.stream.Stream;

public class IntermediateOperations {

    public static void main(String[] args) {

        //filter()
        //Stream.of("welcome", "to", "java").filter(s -> s.length() > 2).forEach(System.out::println);

        //distinct()
        //Stream.of("duck", "duck", "goose").distinct().forEach(System.out::println);

        //skip()
        // Stream.of(10, 20, 30, 40, 50, 60).skip(3).forEach(System.out::println);

        //limit()
        //Stream.of(10, 20, 30, 40, 50, 60).limit(4).forEach(System.out::println);
        //Stream.iterate(1, x -> x + 1).limit(5).forEach(System.out::println);

        //map()
        //Stream.of("duck", "duck", "goose").map(String::length).forEach(System.out::println);

        //flatMap()
        /*Stream<Integer> stream1 = Stream.of(1, 2);
        Stream<Integer> stream2 = Stream.of(3, 4);

        Stream.of(stream1, stream2).flatMap(x -> x).forEach(System.out::println);*/

        /*List<String> zero = List.of();
        var one = List.of("goose");
        var two = List.of(1, 2);

        Stream.of(zero, one, two).flatMap(Collection::stream).forEach(System.out::println);*/

        //sorted()
        //Stream.of(5, 3, 2, 5, 7, 8, 6, 5, 4, 3, 2).sorted().forEach(System.out::println);
        //Stream.of(5, 3, 2, 5, 7, 8, 6, 5, 4, 3, 2).sorted(Comparator.reverseOrder()).forEach(System.out::println);

        //peek()
        long count = Stream.of("welcome", "to", "java").peek(System.out::println).filter(x -> x.length() > 2).count();
        System.out.println("Count = " + count);
    }
}